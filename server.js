var port = 80,	
	bodyParser = require('body-parser'),
	express = require('express'),
    app = express(),
    path = require('path'),
	router = express.Router();
	routerServices = require('./routes/services')

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    res.header('Cache-Control', 'no-cache');

    if ( req.method == 'OPTIONS') { res.sendStatus(200); res.end(); } else { next(); }
})

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static('./webapp'));

router.get('/', function (req, res, next) {
    res.sendFile(path.join(__dirname + '/webapp/index.html'));
});

app.use(router);
app.use('/services', routerServices);


app.listen(port, function () {
    console.log('Servidor iniciado ( port: ' + port + ') - ' + new Date() + '... ');
});