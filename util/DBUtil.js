var mysql = require('mysql'),
    conn = require('./config/connection'),
    connection = mysql.createConnection( conn.getConnection().config );

module.exports = {
    testConn(callback) {
        connection.connect(function(err){
            callback(err)  
        })
    },    
    query( sql, args) {  
        console.log(' - Executando Query: ', sql, args)     
        return new Promise( ( resolve, reject ) => {
            connection.query( sql, args, ( err, rows ) => {
                //console.log('response: ', sql, args, err, rows)
                //DBUtil.close()
                if ( err ) return reject(err)
                resolve(rows[0] );
            } );
        } );
    },
    close() {
        return new Promise( ( resolve, reject ) => {
            connection.end( err => {
                if ( err ) return reject( err );
                resolve();
            } );
        } );
    }
}

