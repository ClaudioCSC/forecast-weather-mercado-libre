exports.getConnection = function () {
    const mysql = require('mysql');
    const dataConfig = null;
    try {
        dataConfig = require('../config.json');
    } catch(e) {}
    

    if(dataConfig != null) {    
        mysql.config = {
            host     : dataConfig.get('HOST_IP'),
            user     : dataConfig.get('MYSQL_USER'),
            password : dataConfig.get('MYSQL_PASSWORD'),
            database : dataConfig.get('DATABASE')
        };    
    } else {
        mysql.config = {
            host     : 'localhost',
            user     : 'root',
            password : 'root',
            database : 'forecastSQL'
        }; 
    }

    return mysql;
}