var express = require('express'),
    routerUser = express.Router(),
    lib = require('../webapp/lib/weather'),
    database = require('../util/DBUtil');

/**
 * @method /clima
 * Servicio REST que calcula el clima de un dia determinado
 * 
 * @param {int} dia Dia que se requiere buscar
 * @param {int} info (Opcional) Si se requiere o no mas info
 * @returns {object} Dia y clima del dia requerido
 */
routerUser.get('/clima', function (req, res) {
    console.log("Calculando clima: ", req.query);
    if(req.query.dia && req.query.dia > 0) {
        var result = lib.weatherCondition(req.query.dia);
        //console.log("result: ", result);   
        var response = {
            "dia": req.query.dia,
            "clima": result.weather
        };        
        if(req.query.info) {
            response.positions = result.planetFeres.pos + "," + 
                result.planetVulca.pos + "," + result.planetBeta.pos;
            if(result.distances) response.distances = result.distances;
            if(result.angles) response.angles = result.angles;
        }    
        //console.log(response); 
        res.send(response);

    } else res.send({"dia": req.query.dia, "clima": "Not found!", "error": "Ingrese parametro dia." });
});

/**
 * @method /climaxperiodo
 * Servicio REST que retorna la informacion del clima en un periodo de tiempo
 * 
 * @param {int} startDay (Opcional) Dia de inicio del periodo a buscar
 * @param {int} endDay (Opcional) Dia de finalizacion del periodo a buscar
 * @param {int} info (Opcional) Si se requiere o no mas info
 * @returns {object} Informacion del periodo a buscar
 */
routerUser.get('/climaxperiodo', function (req, res) {
    console.log("Calculando periodos: ", req.query);
    var startDay = 0, endDay = 0, response = {};
    if(req.query.startDay && req.query.startDay > 0) {
        startDay = parseInt(req.query.startDay);
    } else if(req.query.startDay < 0) res.send({"clima": "Not found!", "error": "Ingrese parametro startDay mayor a 0." });

    if(req.query.endDay && req.query.endDay > 0) {
        endDay = parseInt(req.query.endDay);
        if(req.query.startDay && req.query.startDay > req.query.endDay)
            res.send({"clima": "Not found!", "error": "Ingrese parametro endDay mayor a starDay." });
    } else res.send({"clima": "Not found!", "error": "Ingrese parametro endDay mayor a 0." });

    var response = lib.climaXPeriodo(req.query.startDay , req.query.endDay , req.query.info );
    res.send({"response": response});
});

/**
 * @method /testConnection
 * Servicio REST para testear la conexion con la base de datos
 */
routerUser.get('/database/testConnection', function (req, res) {
    console.log('Probando conexion a Base de datos...');
    database.testConn(function (err) {
        if (!err) {
            console.log("- Database is connected ...");
            res.send({"state": "OK"});            
        }
        else { 
            console.log("- Error connecting database ...", err);
            res.send({"state": "error", "error": err});
        }
    })
});

/**
 * @method /database/clima
 * Servicio REST que obtiene desde la base de datos el clima del dia especificado
 * 
 * @param {int} dia Dia que se requiere buscar
 * @param {int} info (Opcional) Si se requiere o no mas info
 * @returns {object} Dia y clima del dia requerido
 */
routerUser.get('/database/clima', function (req, res) {
    console.log("Obteniendo clima: ", req.query);
    console.log("options: ", options);
    var response = {}
    database.query(
        'SELECT * FROM weather WHERE day = ?',
        [req.query.dia],
        (err, results) => {
          if (err) {
              console.log("- ERROR: " + err);
            res.send({ error: err }); 
            return;
          }
          const hasMore = false;
          res.send({ result: results });          
        }
      );
});

module.exports = routerUser;

