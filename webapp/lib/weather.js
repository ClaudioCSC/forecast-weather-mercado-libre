/** 
 * TEOREMA COSENO a2 = b2 + c2 - 2 * a * b * COS(A°)
        se consideran los siguientes datos:
        
    A = Feres.pos, a = Distancia entre Vulca y Beta
    B = Vulca.pos, b = Distancia entre Feres y Beta
    C = Beta.pos, c = Distancia entre Feres y Vulca
 * 
*/

/**
 * La variacion que pueden tener los angulos al calcular si estan alineados entre si.
 * @property {int}
 */
var variant = 8,

/**
 * Si se requiere log o no
 * @property {boolean}
 */
    log = true;

/**
 * Datos que se tienen del los respectivos planetas
 * @property {object}
 * NOTA: {int} speed: Dato que se utiliza para la velocidad de traslacion en el grafico.
 *       {int} timer: Temporizador usado en el grafico
 */
var planetFeres = {
        name: "Ferengi",
        size: 5,
        strokeSize: 40,
        speed: 57.5,
        traslate: 1,
        sunDistance: 500,
        pos: null,    
        timer: 1
    },
    planetVulca = {
        name: "Vulcano",
        size: 5,
        strokeSize: 60,
        speed: 47.65,
        traslate: 5,
        sunDistance: 1000,
        pos: null,    
        timer:  5
    },
    planetBeta = {
        name: "Betasoide",
        size: 5,
        strokeSize: 80,
        speed: 21.47,
        traslate: 3,
        sunDistance: 2000,
        pos: null,    
        timer:  3
    };

/**
 * @method calculateAngle
 * Calcula la diferencia entre ambos angulos teniendo en cuenta la posicion de estos respecto del eje X.
 * 
 * @param {int} pos1 Grados donde se encuentra el primer planeta
 * @param {int} pos2 Grados donde se encuentra el segundo planeta
 * @returns {int} Diferencia entre ambos angulos de entrada
 */
function calculateAngle(pos1, pos2) {
    if(log) console.log("calculateAngle: " + pos1 + ", " + pos2);
    var angle = 0;
    if((pos1 >= 270 && pos2 < 90) || (pos2 >= 270 && pos1 < 90) ||
        ((pos1 >= 270 && pos2 < 180) || (pos2 >= 270 && pos1 < 180))) {
        
        if(pos1 >= 270) angle = (360 - pos1) + parseInt(pos2);
        else if(pos2 >= 270) angle = (360 - pos2) + parseInt(pos1);
    } else {
        angle = pos1 > pos2 ? pos1 - pos2 : pos2 - pos1;
    }
    if(log) console.log("return: " + angle);
    return angle;
}

/**
 * @method getAngle
 * Calcula el angulo opuesto de a, mediante despeje de la variable en el teorema de cosenos
 * 
 * @param {float} a Distancia del lado a
 * @param {float} b Distandia del lado b
 * @param {float} b Distancia del lado c
 * @returns {int} Grados del angulo opuesto de a
 */
function getAngle(a, b, c) {
    if(log) console.log("getAngle: a = " + a + ", b = " + b + ", c = " + c)
    var a = ((a*a) - (b*b) - (c*c)) / (-(2*b*c));
    if(log) console.log("a: " + a)
    a = parseFloat(Math.acos(a)* (180 / Math.PI)).toFixed(0);
    if(log) console.log("result: " + a)
    return a;
}

/**
 * @method getAnglesOfPlanets
 * Metodo recolector de datos
 * 
 * @param {object} planetFeres Datos del planeta Ferengis
 * @param {object} planetVulca Datos del planeta Vulcano
 * @param {object} planetBeta Datos del planeta Betasoide
 * @returns {object} Distancias y angulos de los planetas
 * NOTA: se considera en las distancias
 *     - {float} a: Distancia entre el planeta Vulcano y el plantena Betasoide
 *     - {float} b: Distancia entre el planeta Ferengis y el plantena Betasoide
 *     - {float} c: Distancia entre el planeta Vulcano y el plantena Ferengis
 * 
 *     - {int} angleA: Angulo opuesto a la distancia a
 *     - {int} angleA: Angulo opuesto a la distancia b
 *     - {int} angleA: Angulo opuesto a la distancia c
 */
function getAnglesOfPlanets(planetFeres, planetVulca, planetBeta) {   

   var angle = calculateAngle(planetVulca.pos, planetBeta.pos);
   var a = getDistance(planetVulca.sunDistance, planetBeta.sunDistance, angle)

   angle = calculateAngle(planetFeres.pos, planetBeta.pos);
   var b = getDistance(planetFeres.sunDistance, planetBeta.sunDistance, angle)

   angle = calculateAngle(planetVulca.pos,  planetFeres.pos);
   var c = getDistance(planetVulca.sunDistance, planetFeres.sunDistance, angle)
   
   if(log) console.log("- Distancias: " + a + ", " + b + ", " + c);

   var angleA = getAngle(a, b, c);
   var angleB = getAngle(b, a, c);
   var angleC = getAngle(c, a, b);

   if(log) console.log("- Angulos: " + angleA + ', ' + angleB + ', ' + angleC);

   return { "distances": [a, b, c], "angles": [angleA, angleB, angleC] };
}

/**
 * @method getData
 * Metodo utilizado para el grafico. Setea timers
 * 
 * @returns {object} Con lo datos de los tres planetas
 */
function getData() {
    planetFeres.timer = planetFeres.traslate;
    planetVulca.timer = planetVulca.traslate;
    planetBeta.timer = planetBeta.traslate;

    return {"planetFeres" : planetFeres, "planetVulca": planetVulca, "planetBeta": planetBeta };
}

/**
 * @method getDistance
 * Calcula la distancia triangular de entre b y c utilizando el teorema de cosenos
 * 
 * @param {int} b Distancia del planeta 1 respecto del sol
 * @param {int} c Distancia del planeta 2 respecto del sol
 * @param {int} gr Angulo que forman entre los 2 planetas
 * @returns {float} Distancia opuesta al angulo a
 */
function getDistance(b, c, gr) {
    if(log) console.log("getDistance: b = " + b + ", c = " + c + ", gr = " + gr)
    var a = (b*b) + (c*c) - 2 * b * c * Math.cos(gr  * Math.PI / 180);
    if(log) console.log("COS: " + Math.cos(gr  * Math.PI / 180))
    if(log) console.log("a =" + a)
    a = parseFloat(Math.sqrt(a)).toFixed(2);
    if(log) console.log("result: " + a);
    return a;
}

/**
 * @method getPointPositions
 * Calcula la posicion de los planetas para un dia determinado
 * 
 * @param {int} day Dia en el que se busca la posicion de los planetas
 * @returns {object} Datos de los planetas
 */
function getPointPositions(day) {
    planetFeres.pos = day;
    while(planetFeres.pos > 360) planetFeres.pos -= 360;

    planetVulca.pos = day * planetVulca.traslate;   
    while(planetVulca.pos > 360) planetVulca.pos -= 360;
    planetVulca.pos = 360 -planetVulca.pos;

    planetBeta.pos = day * planetBeta.traslate;
    while(planetBeta.pos > 360) planetBeta.pos -= 360; 
    
    return { "planetFeres": planetFeres, "planetVulca": planetVulca, "planetBeta": planetBeta };
}

/**
 * @method side
 * Determina si el planeta 2 se encuentra a la derecha o izquierda del planeta 1 respecto del sol.
 * Se considera -1: planeta a la izquierda, 1: planeta a la derecha
 * 
 * @param {int} planet1 Posicion (grados) en el que se encuentra el planeta 1
 * @param {int} planet2 Posicion (grados) en el que se encuentra el planeta 2
 * @returns {int} Posicion del planeta considerando si es a la izquiera o derecha
 */
function side(planet1, planet2) {
    //console.log("side: " + planet1 + "," + planet2)
    var side = 0;
    if(planet1 < 180) {
		if(planet2 > planet1 && planet2 < planet1 +180) side =  -1;
		else if(planet2 <= 360 || (planet2 >= 0 && planet2 < planet1)) side =  1;
	} else {
		var ext = (parseInt(planet1) + 180) - 360;
		if(planet2 < planet1 && planet2 > ext) side =  1
        else if(planet2 > planet1 && planet2 <= 360 || (planet2 >= 0 && planet2 < ext)) 
            side =  -1;
	}
    //console.log("result: " + side);
    return side;
}

/**
 * @method weatherCondition
 * Determina el clima que va a tener un dia determinado
 * 
 * @param {int} day Dia que se quiere saber el clima
 * @param {boolean} logger (opcional) Si se quiere o no ver el log
 * @returns {object} Informacion de los planetas incluido el clima del dia, distancias y angulos obtenidos
 */
function weatherCondition (day, logger) {
    var weather = "Normal",
        result = {
            "planetFeres" : planetFeres, 
            "planetVulca": planetVulca, 
            "planetBeta": planetBeta, 
            "weather": weather
        };
    log = logger;
    getPointPositions(day);

    // Alineacion respecto al sol
    if(planetFeres.pos%90 == 0 && planetVulca.pos%90 == 0 && planetBeta.pos%90 == 0) {
        result.weather = "Sequia"       
    } else { 
        //Alineacion respecto sin sol
        var angs = getAnglesOfPlanets(planetFeres, planetVulca, planetBeta) 
        var optimo = 0;
        angs.angles.forEach(function(a) {
            if(a <= variant) optimo++;
        })
        if(optimo == 2) {
            result.weather = "Optimo";      
        } else {
            //Triangulacion
            if(log) console.log("Triangulacion...");
            if(log) console.log("- Feres/Vulca: " + side(planetFeres.pos, planetVulca.pos) + " - Feres/Beta: " + side(planetFeres.pos, planetBeta.pos));
            if(side(planetFeres.pos, planetVulca.pos) == -1 && side(planetFeres.pos, planetBeta.pos) == 1) {
                if(log) console.log("- Vulca/Feres: " + side(planetVulca.pos, planetFeres.pos) + " - Vulca/Beta: " + side(planetVulca.pos, planetBeta.pos));
                if(side(planetVulca.pos, planetFeres.pos) == 1 && side(planetVulca.pos, planetBeta.pos) == -1) {
                    if(log) console.log("- Beta/Feres: " + side(planetBeta.pos, planetFeres.pos) + " - Beta/Vulca: " + side(planetBeta.pos, planetVulca.pos));
                    if(side(planetBeta.pos, planetFeres.pos) == -1 && side(planetBeta.pos, planetVulca.pos) == 1) {
                        result.weather = "Lluvia"; 
                    }
                }
            } else {
                if(log) console.log("- Feres/Vulca: " + side(planetFeres.pos, planetVulca.pos) + " - Feres/Beta: " + side(planetFeres.pos, planetBeta.pos));
                if(side(planetFeres.pos, planetVulca.pos) == 1 && side(planetFeres.pos, planetBeta.pos) == -1) {
                    if(log) console.log("- Vulca/Feres: " + side(planetVulca.pos, planetFeres.pos) + " - Vulca/Beta: " + side(planetVulca.pos, planetBeta.pos));
                    if(side(planetVulca.pos, planetFeres.pos) == -1 && side(planetVulca.pos, planetBeta.pos) == 1) {
                        if(log) console.log("- Beta/Feres: " + side(planetBeta.pos, planetFeres.pos) + " - Beta/Vulca: " + side(planetBeta.pos, planetVulca.pos));
                        if(side(planetBeta.pos, planetFeres.pos) == 1 && side(planetBeta.pos, planetVulca.pos) == -1) {
                            result.weather = "Lluvia"; 
                        }
                    }
                }
            }            
        }
        result.distances = angs.distances[0] + "," + angs.distances[1] + "," + angs.distances[2];
        result.angles = angs.angles[0] + "," + angs.angles[1] + "," + angs.angles[2];
    }

    return result;
}

/**
 * @method climaXPeriodo
 * Metodo recolector del informacion
 * 
 * @param {int} startDay Dia inicial del periodo a buscar
 * @param {int} endDay Dia en que finaliza el periodo a buscar
 * @param {boolean} info (opcional) Si se requiere mas info o no
 * @returns {object}
 */
function climaXPeriodo(startDay, endDay, info) {
    var longDays = startDay ? endDay - startDay : endDay,
        arr = [];

    for(var i =  startDay ?  startDay : 0; i <= longDays; i++) {
        arr.push(weatherCondition(i));
    }

    return analizeWeather(arr, info);
}

/**
 * @method analizeWeather
 * Calcula la cantidad de veces que ocurre un evento climatico e incluye dia de inicio y final de cada periodo
 * 
 * @param {object[]} arr Datos de los eventos climaticos por dia
 * @param {boolean} info (opciona) Si se requiere o no mas info
 * @returns {object} Recopila la info de los eventos climaticos
 */
function analizeWeather(arr, info) {
    var state = { "weather": ""},        
        response = [];
    for(var i = 0; i < arr.length; i++) {
        var day = arr[i];
        //console.log("Dia: " + i + " - " + day.weather)
        if(day.weather != state.weather) {
            var resp = null
            for(var j = 0; j < response.length; j++) {
                if(response[j].weather == day.weather) resp = response[j];
            }
            if(resp == null) {
                resp = { "weather": day.weather, "periodsCount": 1, "periods": [
                        {"days": 1, "starDay": i, "endDay": i }
                    ] };                
                response.push(resp);
            } else { 
                resp.periods.push({"days": i, "starDay": i, "endDay": i })
                resp.periodsCount++;
            }
            state = resp;
        } else {
            state.periods[state.periods.length-1].days++;
            state.periods[state.periods.length-1].endDay = i;
            state.periodsCount = state.periods.length;
        }
    }
    
    for(var j = 0; j < response.length; j++) {
        if(!info) delete response[j].periods;
        if(response[j].weather == "Lluvia") {
            response[j].maxRainDay = findMaxDayOfRain(arr);
        }
    } 
    return response;
}

/**
 * @method findMaxDayOfRain
 * Calcula el dia en que mas lluvia habra en un periodo de tiempo
 * 
 * @param {object[]} arr Datos de los eventos climaticos por dia
 * @returns {int} Dia en que habra un pico maximo de lluvia
 */
function findMaxDayOfRain(arr) {
    var maxA1 = 0, maxA2 = 0, maxA3 = 0, maxADay = 0,
        maxD1 = 0, maxD2 = 0, maxD3 = 0, maxDDDay = 0;
    for(var i = 0; i < arr.length; i++) {
        var day = arr[i];
        if(day.weather == "Lluvia") {
            var angles = day.angles.split(",");
            if(angles[0] > maxA1 && angles[1] > maxA2 && angles[2] > maxA3) {
                maxA1 = angles[0];
                maxA2 = angles[1];
                maxA3 = angles[2];
                maxADay = i;
            }
            
            /*var distances = day.distances.split(",");
            if(distances[0] > maxD1 && distances[1] > maxD2 && distances[2] > maxD3) {
                maxD1 = distances[0];
                maxD2 = distances[1];
                maxD3 = distances[2];
                maxDDay = i;
            }*/
        }
    }
    //console.log("Dia: " + maxADay + " - Pico maximo de lluvia en Angulos");
    //console.log("Dia: " + maxDDay + " - Pico maximo de lluvia en Distancias");
    return maxADay;
}

try {
    var library = {};
    library.getPointPositions = function(day) {
        return getPointPositions(day);
    };
    library.weatherCondition = function(day) {
        return weatherCondition(day);
    };
    library.climaXPeriodo = function(startDay, endDay, info) {
        return climaXPeriodo(startDay, endDay, info);
    };
    module.exports = library;
} catch(e) {}

