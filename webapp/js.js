var days = 0,
    animation = false;

var planetFeres = null,
    planetVulca = null,
    planetBeta = null;

window.onload = function () {
    var canvas = document.getElementById("canvas"),
    ctx = canvas.getContext("2d");

    //PARAMETERS
    var swidth = 200,
        sheight = 200;
    
    var sunSize = 20;    

    canvas.width = swidth;
    canvas.height = sheight;
    canvas.style = "background: white;";

    var data = getData();
    planetFeres = data.planetFeres;
    planetVulca = data.planetVulca;
    planetBeta = data.planetBeta;

    animate();

    /*** CREATE SUN ***/
    function sun(radius) {
        ctx.beginPath();
        ctx.arc(0, 0, radius , 0, 2 * Math.PI, true);
        ctx.fillStyle = 'yellow';
        ctx.fill();
        ctx.strokeStyle = 'orange';
        ctx.stroke();
        ctx.closePath();
    }

    /*** CREATE PLANETS ***/
    function planet(x, y, radius, color, strokeRadius) {
        traslationStroke(strokeRadius);
        ctx.translate(strokeRadius,0);

        ctx.beginPath();
        ctx.arc(x, y, radius , 0, 2 * Math.PI, true);
        ctx.fillStyle = color;
        ctx.fill();
        ctx.closePath();

        ctx.translate(-strokeRadius,0);
    }

    /*** CREATE THE PLANET STROKE ***/
    function traslationStroke(radius) {
        ctx.beginPath();
        ctx.arc(0, 0, radius, 0, 2*Math.PI, true);
        ctx.lineWidth = "1";
        ctx.strokeStyle = 'gray';
        ctx.stroke();
        ctx.closePath();
    }

    /*** ANIMATION OF PLANETS ***/
    function animate() {
        ctx.save();
        ctx.beginPath();
        ctx.fillStyle = "white";
        if(planetFeres.timer < days)  ctx.fillRect(0,0,swidth,sheight);
        ctx.closePath();

        ctx.translate(swidth/2,sheight/2);
        sun(sunSize);         

        //FERES
        if(planetFeres.timer <= days) {
            if(animation) planetFeres.timer += planetFeres.traslate;
            else planetFeres.timer = days;
            ctx.rotate(planetFeres.timer/planetFeres.speed); 
        }
        planet(0,0, planetFeres.size,"#898989", planetFeres.strokeSize); 
        
        //VULCA
        if(planetVulca.timer <= days * planetVulca.traslate) {
            if(animation) planetVulca.timer += planetVulca.traslate;
            else planetVulca.timer = days * planetVulca.traslate;
            ctx.rotate((planetVulca.timer / planetVulca.speed) * -1);            
        }
        ctx.rotate(0);
        planet(0,0, planetVulca.size,"#898989", planetVulca.strokeSize);

        //BETA
        if(planetBeta.timer <= days* planetBeta.traslate) {
            if(animation) planetBeta.timer += planetBeta.traslate;
            else planetBeta.timer = days * planetBeta.traslate
            ctx.rotate(planetBeta.timer/ planetBeta.speed);
        }
        planet(0,0, planetBeta.size,"#898989", planetBeta.strokeSize); 
    
        ctx.restore();
        if(planetFeres.timer < days) requestAnimationFrame(animate);
        else cancelAnimationFrame(animate);
    }
}

/**
 * @method retroceso
 * Metodo que inicia la animacion desde el dia 0 al dia ingresado.
 * Invocado por el boton con la descripcion "Ver recorrido"
 */
function startAnimation() {
    animation = true;
    days = document.getElementById("days").value;
    window.onload();
}

/**
 * @method avanzar
 * Metodo que muestra el movimiento de los planetas al dia siguiente del ingresado
 * Invocado por el boton con la descripcion ">>"
 * 
 * @param {boolean} retrocede (Opcional) Determina si se retrocede o avanza
 */
function avanzar(retrocede) {
    animation = false;
    days = document.getElementById("days").value;
    if(retrocede) days--;
    else days++;
    document.getElementById("days").value = days;
    window.onload();
}

/**
 * @method retroceso
 * Metodo que invoca el metodo avanzar pidiendole que retroceda el grafico un dia menos al ingresado
 * Invocado por el boton con la descripcion "<<"
 */
function retroceso() {    
    avanzar(true)
}

/**
 * @method viewCoordenadas
 * Metodo que muestra en consola las posiciones de los planetas
 * 
 * @param {int} day Dia que se quiere saber las posiciones de los planetas
 */
function viewCoordenadas(day) {
    console.log('View coordenadas dia: ' + day);

    getPointPositions(day);

    console.log('- Positions: ' + planetFeres.pos + ',' + planetVulca.pos + ',' + planetBeta.pos);
}

/**
 * @method viewWeather
 * Metodo muestra en pantalla y en consola la posicion de los planetas.
 * Invocado por el boton con la descripcion "Ver clima del dia"
 */
function viewWeather() {
    var day = document.getElementById("days").value;
    window.onload();
    var log = true;
    viewCoordenadas(day);
    
    var result = weatherCondition(day, log);
    console.log("- Clima: " + result.weather)
    document.getElementById("response").innerHTML = "El clima para el dia " + day + " es: " + result.weather;
    if(!log && result.distances) console.log("- Distancias: " + result.distances);
    if(!log && result.angles) console.log("- Angulos: " + result.angles)
}

/**
 * @method viewWeather
 * Metodo muestra en pantalla la cantidad de periodos climaticos que ocurriran en los proximos 10 años.
 * Invocado por el boton con la descripcion "Pronostico 10 años"
 */
function viewTenYear() {
    var response = climaXPeriodo(0, 3600, false),
        html = "En 10 años van a haber: </p>";
    response.forEach(function(period) {
        html += "- Periodos " + period.weather + ": " + period.periodsCount + "</p>";
        if(period.weather == "Lluvia") html += " Dia maximo de lluvia: " + period.maxRainDay + "</p>"
    })

    document.getElementById("response").innerHTML = html;
}