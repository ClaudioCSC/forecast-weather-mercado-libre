# Weather Forecast

# **Descripción**

Aplicación realizada en **_Node JS_** que muestra un gráfico con 3 planetas de una galaxia lejana, si usted **_ingresa_** un número de día en particular, se mostrará el clima que tendrán los planetas para
el día ingresado. Adicionalmente podrá ver una estimación del clima en los próximos 10 años.
Estos mismos resultados también podrá obtenerlos mediantes servicios **_REST_**

# **Instalación**

Inicialmente ejecutar **_npm install_**, para descargar las librerías solicitadas. Ejecutar **_node server_** y accediendo, en tu navegador, a la URL **http://localhost** podrás ver la versión grafica de la galaxia y podrán consultar el clima en un día particular.

# **Servicios REST**
También  se provee 2 servicios REST de consulta:

- **_Consulta del clima en un día particular_**
    http://localhost/services/clima?dia=<Dia_Consulta>

- **_Consulta del clima desde el día cero al día ingresado_**
    http://localhost/services/climaxperiodo?endDay=<Dia_Consulta>

NOTA: Ambos servicios opcionalmente se les puede agregar el parámetro  **info=true** para obtener más información.


